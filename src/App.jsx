import { useSelector } from "react-redux";
import React from "react";

import AllRoutes from "./config/routes/AllRoutes";

import "./App.css";

const App = (props) => {
  const isLoading = useSelector((state) => state.auth.isLoading);

  return (
    <div>
      <AllRoutes />
    </div>
  );
};

export default App;

const Images = {
    Logo: require("../../assets/images/logo.png"),
    DefaultUser: require("../../assets/images/profile.png")
}

export default Images;
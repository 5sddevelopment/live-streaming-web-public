import { Navigate } from "react-router";

import Home from "../../containers/Home/index";
import LiveRoom from "../../containers/LiveRoom";
import Login from "../../containers/Login";
import PreWaitingRoom from "../../containers/PreWaitingRoom";
import Signup from "../../containers/Signup";
import WaitingRoom from "../../containers/WaitingRoom";

export const routes = {
  protected: {
    home: "/home",
    live: "/live-streaming",
    waiting: "/waiting-room",
    preWaiting: "/pre-waiting-room"
  },
  public: {
    login: "/login",
    signup: "/signup",
  },
};

export const PROTECTED_ROUTES = [
  {
    path: routes.protected.home,
    exact: false,
    component: <Home />,
  },
  {
    path: routes.protected.live,
    exact: false,
    component: <LiveRoom />,
  },
  {
    path: routes.protected.waiting,
    exact: false,
    component: <WaitingRoom />,
  },
  {
    path: routes.protected.preWaiting,
    exact: false,
    component: <PreWaitingRoom />,
  },
  {
    path: "*",
    exact: false,
    component: <Navigate to={routes.protected.home} />,
  },
];

export const PUBLIC_ROUTES = [
  {
    path: routes.public.login,
    exact: false,
    component: <Login />,
  },
  {
    path: routes.public.signup,
    exact: false,
    component: <Signup />,
  },
  {
    path: "*",
    exact: false,
    component: <Navigate to={routes.public.login} />,
  },
];

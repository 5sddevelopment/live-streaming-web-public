import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Route, Routes } from "react-router-dom";

import { AppAction } from "../../store/actions";
import { PROTECTED_ROUTES } from "./siteMap";

const AppRoutes = (props) => {
  const dispatch = useDispatch();
  const { hostSocket, hostPeerConnection } = useSelector((state) => state.app);

  // useEffect(() => {
  //   return () => {
  //     if (hostSocket) {
  //       hostSocket.disconnect();
  //       dispatch(AppAction.setHostSocket(null));
  //       dispatch(AppAction.setHostPeer(null));
  //     }
  //     alert("socket disconnected");
  //   };
  // }, []);

  return (
    <Routes>
      {PROTECTED_ROUTES.map((route) => (
        <Route
          key={route.path}
          path={route.path}
          element={route.component}
          exact={route.exact}
        />
      ))}
    </Routes>
  );
};

export default AppRoutes;

// var ip = "192.168.18.161"
// var port = "8000"
// var baseUrl = `http://${ip}:${port}/api/`;

var baseUrl = "https://gm10.5stardesigners.net/sockets-and-laravel/api/"
var ip = "https://acktive.5stardesigners.net"
var port = "3008"

// var ip = "192.168.15.137";
// var port = "3001";

var socketUrl = `${ip}:${port}`
// var socketUrl = `https://gm10.5stardesigners.net:3002`


const configVariables = {
    port,
    baseUrl,
    socketUrl
}

const constantVariables = {
    // TempUser: { name: "David Smith", email: "david@yopmail.com", pass: "asd123", userId: 12 }
}

export default {
    ...configVariables,
    ...constantVariables
}
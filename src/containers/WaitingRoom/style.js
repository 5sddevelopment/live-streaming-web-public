const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: "10px 0px",
    position: "relative",
    overflowX: "hidden",
  },
  videoContainer: {
    position: "relative",
    width: "100%",
    display: "flex",
    flexDirection: "row",
    marginBlock: 10,
    overflowX: "auto",
  },
  videoBox: {
    position: "relative",
    marginInline: "20px",
  },
  header: {
    width: "100%",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  buttonsView: {
    display: "flex",
    marginBlock: 10,
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "center",
  },
  button: {
    margin: 5,
  },
  endButton: {
    height: "40px",
    backgroundColor: "red",
    color: "white",
    borderRadius: 10,
    outline: "none",
    border: "none",
    padding: "7px",
    fontWeight: "600",
    marginRight: "20px",
  },
  backButton: {
    backgroundColor: "#251C8B",
    height: "40px",
    color: "white",
    borderRadius: 10,
    outline: "none",
    border: "none",
    padding: "7px 10px",
    fontWeight: "600",
    marginLeft: "20px",
    cursor: "pointer"
  },
  acceptButton: {
    backgroundColor: "#251C8B",
    color: "white",
    borderRadius: 10,
    outline: "none",
    border: "none",
    padding: "10px 20px",
    fontWeight: "600",
    marginRight: "20px",
    cursor: "pointer"
  },
  rejectButton: {
    backgroundColor: "#FF2762",
    color: "white",
    borderRadius: 10,
    outline: "none",
    border: "none",
    padding: "10px 20px",
    fontWeight: "600",
    cursor: "pointer"
  },
  streamName: {
    position: "absolute",
    top: 20,
    left: 20,
    backgroundColor: "rgba(0,0,0,.5)",
    padding: "5px 10px",
    color: "white",
    borderRadius: "15px",
  },
  textCenter: {
    fontSize: "20px",
    width: "100%",
    textAlign: "center",
  },
};

export default styles;

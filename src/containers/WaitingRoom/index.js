import { default as React, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";

import { AppAction } from "../../store/actions";
import { Button } from "../../components";
import { Colors } from "../../config";
import styles from "./style";

function WaitingRoom(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const user = useSelector((state) => state.auth.user);
  const { hostSocket, hostPeerConnection, userStreams, liveUserStreams } =
    useSelector((state) => state.app);

  let videoRef = [];

  let endStreaming = async () => {
    videoRef = [];
    dispatch(AppAction.setUserStream(null));
    hostSocket.emit("host_disconnected");
    console.log("You are disconnected!");
    navigate("/home");
  };

  const acceptMember = (item) => {
    console.log("liveUserStreams ", liveUserStreams);
    if (liveUserStreams.length === 8) {
      alert("Live room is full");
    } else {
      hostSocket.emit("user_live", item.socketId);
      const body = {
        socketId: item.socketId,
        status: true,
      };
      dispatch(AppAction.setUserStatus(body));
    }
  };

  const rejectMember = (item) => {
    hostSocket.emit("user_remove", item.socketId);
    const body = {
      socketId: item.socketId,
      status: false,
    };
    dispatch(AppAction.setUserStatus(body));
  };

  useEffect(() => {
    return () => {
      videoRef = [];
    };
  }, []);

  useEffect(() => {
    videoRef.forEach((val, ind) => {
      videoRef[ind].srcObject = userStreams[ind]?.mediaStreams;
    });
  }, [videoRef]);

  return (
    <div style={styles.container}>
      <div style={styles.header}>
        <Button
          text="Back"
          handleClick={() => navigate("/home")}
          style={styles.backButton}
        />
        <h1>{props.stream_name}Waiting Room</h1>
        <Button
          text="End Streaming"
          handleClick={endStreaming}
          style={styles.endButton}
        />
      </div>
      <div style={styles.videoContainer}>
        {userStreams.length > 0 ? (
          userStreams?.map((val, ind) => (
            <div style={styles.videoBox} key={ind}>
              <video
                ref={(ref) => (videoRef[ind] = ref)}
                width={250}
                height={445}
                style={{ backgroundColor: Colors.Black, borderRadius: "10px" }}
                autoPlay
                muted
              ></video>
              <div style={styles.streamName}>{val?.userName}</div>
              <div style={styles.buttonsView}>
                <Button
                  text="Accept"
                  style={styles.acceptButton}
                  onClick={() => acceptMember(val)}
                />
                <Button
                  text="Reject"
                  style={styles.rejectButton}
                  onClick={() => rejectMember(val)}
                />
              </div>
            </div>
          ))
        ) : (
          <div style={styles.textCenter}>No members in Waiting Room</div>
        )}
      </div>
    </div>
  );
}

export default WaitingRoom;

import { default as React, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";

import { Button } from "../../components";
import { Colors } from "../../config";
import { AppAction } from "../../store/actions";
import styles from "./style";

function LiveRoom(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.auth.user);
  const { hostSocket, liveUserStreams } = useSelector((state) => state.app);

  let videoRef = [];
  let endStreaming = async () => {
    videoRef = [];
    dispatch(AppAction.setUserStream(null));
    hostSocket.emit("host_disconnected");
    console.log("You are disconnected!");
    navigate("/home");
  };

  const rejectMember = (item) => {
    hostSocket.emit("user_remove", item.socketId);
    const body = {
      socketId: item.socketId,
      status: false,
    };
    dispatch(AppAction.setUserStatus(body));
  };

  useEffect(() => {
    return () => {
      videoRef = [];
    };
  }, []);

  useEffect(() => {
    videoRef.forEach((val, ind) => {
      videoRef[ind].srcObject = liveUserStreams[ind]?.mediaStreams;
    });
  }, [videoRef]);

  return (
    <div style={styles.container}>
      <div style={styles.header}>
        <Button
          text="Back"
          handleClick={() => navigate("/home")}
          style={styles.backButton}
        />
        <h1>{props.stream_name}Live Room</h1>
        <Button
          text="End Streaming"
          handleClick={endStreaming}
          style={styles.endButton}
        />
      </div>
      <div style={styles.videoContainer}>
        {liveUserStreams.length > 0 ? (
          liveUserStreams?.map((val, ind) => (
            <div style={styles.videoBox} key={ind}>
              <video
                ref={(ref) => (videoRef[ind] = ref)}
                width={200}
                height={350}
                style={{ backgroundColor: Colors.Black, borderRadius: "10px" }}
                autoPlay
                muted
              ></video>
              <div style={styles.streamName}>{val?.userName}</div>
              <Button
                text="x"
                style={styles.removeButton}
                onClick={() => rejectMember(val)}
              />
            </div>
          ))
        ) : (
          <div style={styles.textCenter}>No members in Live Room</div>
        )}
      </div>
    </div>
  );
}

export default LiveRoom;

import { default as React } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import { AntdTable, Button } from "../../components";
import { AppAction } from "../../store/actions";

import styles from "./style";

function PreWaitingRoom(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { hostSocket, userRequests } = useSelector((state) => state.app);

  let endStreaming = async () => {
    dispatch(AppAction.setUserStream(null));
    hostSocket.emit("host_disconnected");
    console.log("You are disconnected!");
    navigate("/home");
  };

  const handleApproveUser=(id) => {
    hostSocket.emit("user_approve", id);
  }

  const handleRejectUser=(id)=>{
    hostSocket.emit("user_reject", id);
  }

  const columns = [
    {
      title: "Avatar",
      dataIndex: "profile_image",
      key: "profile_image",
      render: (text, record) => (
        <span key={record?.userId}>
          <img
            src={`https://acktive.5stardesigners.net/aktive-poc/public/${text}`}
            style={{
              width: "50px",
              height: "50px",
              objectFit: "cover",
              borderRadius: "25px",
            }}
          />
        </span>
      ),
    },
    {
      title: "Name",
      dataIndex: "displayName",
      key: "displayName",
    },
    {
      title: "Email",
      dataIndex: "userEmail",
      key: "userEmail",
    },
    {
      title: "Action",
      key: "action",
      render: (text, record) => (
        <span style={styles.buttonsView} key={record?.userId} >
          <Button text="Accept" style={styles.acceptButton} onClick={()=>handleApproveUser(record?.userId)} />
          <Button text="Reject" style={styles.rejectButton} onClick={()=>handleRejectUser(record?.userId)} />
        </span>
      ),
    },
  ];

  return (
    <div style={styles.container}>
      <div style={styles.header}>
        <Button
          text="Back"
          handleClick={() => navigate("/home")}
          style={styles.backButton}
        />
        <h1>{props.stream_name}Pre Waiting Room</h1>
        <Button
          text="End Streaming"
          handleClick={endStreaming}
          style={styles.endButton}
        />
      </div>
      <AntdTable
        data={userRequests}
        style={{
          marginTop: "20px",
          width: "80%",
        }}
        columns={columns}
        pagination={false}
      />
    </div>
  );
}

export default PreWaitingRoom;

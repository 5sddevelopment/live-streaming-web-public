import { Col, Form, Input, Row } from "antd";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import React, { useState } from "react";

import { AuthAction } from "../../store/actions";
import { Button, Text } from "../../components";
import { Colors, Fonts, Utils } from "../../config";
import { PoppinsBold } from "../../config/fonts";
import { routes } from "../../config/routes/siteMap";

const Login = (props) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const form = Form.useFormInstance();

  const [state, setState] = useState({
    email: "",
    password: "",
    emailErrMsg: "",
    passErrMsg: "",
    iconClick: false,
    validEmail: true,
    validPass: true,
  });

  const compulsoryInputProps = {
    validateTrigger: "onBlur",
    requiredMark: "optional",
  };

  const signin = (values) => {
    console.log("values", values);
    const body = {
      ...values,
      platform: "web",
    };
    dispatch(
      AuthAction.SignIn(body, () => {
        navigate(routes.protected.home);
      })
    );
    // }
  };

  return (
    <Row justify="center" style={{ minHeight: "100vh" }} align="middle">
      <Col
        sm={24}
        xs={24}
        md={16}
        lg={12}
        xl={8}
        xxl={8}
        align="middle"
        style={{ border: "1px solid black" }}
      >
        <Row
          justify="center"
          gutter={[0, 24]}
          style={{ width: "100%", marginBlock: "35px" }}
        >
          <Col
            style={{
              display: "flex",
              justifyContent: "center",
              flexDirection: "column",
            }}
          >
            <Text
              fontSize={30}
              fontFamily={Fonts[PoppinsBold]}
              color={Colors.Black}
              fontWeight={600}
              textALign={"center"}
              text={"LOGIN"}
            />
            <Text
              fontSize={15}
              fontFamily={Fonts["Poppins-Regular"]}
              color={Colors.LightTextColor}
              fontWeight={400}
              textALign={"center"}
              text={"Welcome back! login with your Email Address.."}
            />
          </Col>

          <Form
            colon={false}
            scrollToFirstError
            onFinish={signin}
            onFinishFailed={() => console.log("Failed")}
          >
            <Form.Item
              label="Email"
              name="email"
              rules={[
                { required: true, message: "Please input your email!" },
                {
                  type: "email",
                  message: "The input is not valid E-mail!",
                },
              ]}
              {...compulsoryInputProps}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[
                { required: true, message: "Please input your password!" },
                {
                  pattern: Utils.passwordRegex,
                  message:
                    "Password length must be atleast 8 char and must include an upper case and lower case char",
                },
              ]}
              {...compulsoryInputProps}
            >
              <Input.Password />
            </Form.Item>
            <Col span={18}>
              <Form.Item>
                <Button
                  style={{
                    backgroundColor: "#FFB001",
                    textTransform: "capitalize",
                    color: Colors.Black,
                    width: "100%",
                    borderRadius: 8,
                    borderColor: Colors.Transparent,
                    cursor: "pointer"
                  }}
                  type="submit"
                  htmlType="submit"
                  text="Sign In"
                />
              </Form.Item>
            </Col>
          </Form>
          {/* <Col span={24}>
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                width: "auto",
              }}
              onClick={() => navigate("/forgot-password")}
            >
              <Text
                text={"Forgot Password?"}
                style={{
                  cursor: "pointer",
                  marginRight: "5px",
                }}
              />
              <span
                style={{
                  fontSize: "15px",
                  fontWeight: "600",
                  cursor: "pointer",
                }}
              >
                Reset
              </span>
            </div>
          </Col> */}
        </Row>
      </Col>
    </Row>
  );
};

export default Login;
{
  /* <Col
            span={18}
            style={{
              marginTop: "10px",
              display: "flex",
              flexDirection: "column",
              justifyContent: "center",
            }}
          >
            <TextField.CustomInput
              label="Email Address"
              size="large"
              type={"email"}
              defaultValue={state.email}
              placeholder={"hello@project.com"}
              suffix={<AiOutlineUser />}
              style={{
                border: "1px solid #F5F5F5",
                boxShadow: "0px 3px 6px #0000001A",
                borderRadius: 10,
              }}
              onChange={(e) => validateEmail(e.target.value)}
            />
            <Text.ErrorText text={state.emailErrMsg} />
          </Col>

          <Col
            className="passwordField"
            span={18}
            style={{
              display: "flex",
              flexDirection: "column",
            }}
          >
            <TextField.CustomInput
              password
              label="Password"
              size="large"
              type={"password"}
              defaultValue={state.password}
              placeholder={"**********"}
              style={{
                border: "1px solid #F5F5F5",
                boxShadow: "0px 3px 6px #0000001A",
                borderRadius: 10,
              }}
              onChange={(e) => validatePass(e.target.value)}
            />
            <Text.ErrorText text={state.passErrMsg} />
          </Col> */
}

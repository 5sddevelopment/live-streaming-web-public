import Home from "./Home";
import Login from "./Login";
import Signup from "./Signup";
import LiveRoom from "./LiveRoom";
import WaitingRoom from "./WaitingRoom";
import PreWaitingRoom from "./PreWaitingRoom";

export default{
    Login: Login,
    Signup: Signup,
    Home: Home,
    LiveRoom: LiveRoom,
    WaitingRoom: WaitingRoom,
    PreWaitingRoom: PreWaitingRoom
}
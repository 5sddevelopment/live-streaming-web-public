import { Peer } from "peerjs";
import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";

import { io } from "socket.io-client";

import { Button } from "../../components";
import { Variables } from "../../config";
import { store } from "../../store";
import { AppAction } from "../../store/actions";
import styles from "./style";

const { socketUrl } = Variables;

function Home() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { hostSocket, hostPeerConnection, userStreams } = useSelector(
    (state) => state.app
  );

  const localPeer = useRef();

  let server = {
    iceServers: [
      {
        urls: "stun:stun.5stardesigners.net:5742",
      },
      // {
      //   urls: 'turn:turn.5stardesigners.net:5742',
      //   username: '5sstunad',
      //   credential: '5tun68475s@ppd3'
      // },
    ],
  };

  let startStreaming = async (socket) => {
    try {
      localPeer.current = new Peer(server);
      dispatch(AppAction.setHostPeer(localPeer.current));
      localPeer.current.on("error", console.log);
      localPeer.current.on("open", (localPeerId) => {
        console.log("Local peer open with ID " + localPeerId);
        console.log("socket id => ", socket.id);
        localPeer.current.on("call", (call) => {
          console.log("receiving call from remote peer id => ", call?.peer);
          call.answer();
          call.on("stream", (str) => {
            console.log({ str });
            socket.emit("get_socket_id", {
              peer_id: call?.peer,
            });
            socket.on("get_socket_id", (data) => {
              console.log("----------running ",data)
              const socketId = data.socket_id;
              const userName = data.user_name;
              const body = {
                mediaStreams: str,
                isLive: false,
                peerId: call?.peer,
                socketId: socketId,
                userName: userName,
              };
              dispatch(AppAction.setUserStream(body));
              socket.emit("user_joined", socketId);
            });
            // mediaStream.push(str)
            // setStreamsLength(mediaStream.length)
          });
        });
        socket.emit("host_connected", {
          peer_id: localPeerId,
        });
      });

      console.log("You are now connected!");
    } catch (err) {
      console.log(err);
      alert(err.message);
    }
  };

  useEffect(() => {
    dispatch(AppAction.LoaderTrue());
    if (
      !store.getState().app.hostSocket ||
      !store.getState().app.hostPeerConnection
    ) {
      const socket = io(socketUrl, {
        transports: ["polling"],
        secure: true,
        autoConnect: true,
      });
      socket.on("connect", () => {
        console.log("socket connected", socket.id);
        socket.emit("host_socket_connected");
        startStreaming(socket);

        dispatch(AppAction.setHostSocket(socket));
        dispatch(AppAction.LoaderFalse());
      });

      socket.on("disconnect", () => {
        console.log("socket disconnected");
        // console.log("socket connected", socket.id);
        // io(socketUrl, {
        //   transports: ["polling"],
        //   secure: true,
        //   autoConnect: true,
        // });

        // setTimeout(() => {}, 15000);
        // console.log(store.getState().app.hostPeerConnection, 'HOST PEER CONENCTION');
        // // socket.emit("host_socket_connected");
        // if (store.getState().app.hostPeerConnection) {
        //   socket.emit("host_connected", {
        //     peer_id: store.getState().app.hostPeerConnection._id,
        //   });
        // }
        // dispatch(AppAction.setHostSocket(socket));
        // dispatch(AppAction.LoaderFalse());
      });
      socket.on("user_disconnected", (socketId, userId) => {
        console.log("res ",socketId, userId)
        dispatch(AppAction.updateRemoveUserStream(socketId, userId));
      });
      socket.on("user_request", (res) => {
        dispatch(AppAction.userRequest(res));
      });
      socket.on("cancel_request", (res) => {
        dispatch(AppAction.updateUserRequest(res));
      });
      socket.on("user_approve", (res) => {
        dispatch(AppAction.updateUserRequest(res));
      });
      socket.on("user_reject", (res) => {
        dispatch(AppAction.updateUserRequest(res));
      });
    }
  }, []);

  console.log();

  return (
    <div style={styles.container}>
      <Button
        text={"Pre Waiting Room"}
        handleClick={() => navigate("/pre-waiting-room")}
        style={styles.otherButton}
      />
      <Button
        text={"Waiting Room"}
        handleClick={() => navigate("/waiting-room")}
        style={styles.primaryButton}
      />
      <Button
        text={"Live Room"}
        handleClick={() => navigate("/live-streaming")}
        style={styles.secondaryButton}
      />
    </div>
  );
}

export default Home;

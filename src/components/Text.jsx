import React from "react";
import styled from "styled-components";

import { Colors } from "../config";

function Text(props) {
  return <TextComponent {...props}>{props.text}</TextComponent>;
}

export default Text;

const TextComponent = styled.span`
  font-size: ${(props) => props.fontSize || 24}px;
  color: ${(props) => props.color || Colors.TextColor};
  font-weight: ${(props) => props.fontWeight || 400};
  text-align: ${(props) => props.textAlign || "center"};
`;

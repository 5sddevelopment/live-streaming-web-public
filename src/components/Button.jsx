import React from "react";

function Button({ text, type, handleClick, style, disabled, ...props }) {
  return (
    <button
      onClick={handleClick}
      style={style}
      type={type}
      {...props}
    >
      {text}
    </button>
  );
}

export default Button;

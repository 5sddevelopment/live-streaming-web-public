import { Table } from "antd";
import React from "react";

function AntdTable({ columns, data, style, bordered, pagination }) {
  return (
    <Table
      columns={columns}
      dataSource={data}
      style={style}
      bordered={bordered}
      pagination={pagination}
    />
  );
}

export default AntdTable;

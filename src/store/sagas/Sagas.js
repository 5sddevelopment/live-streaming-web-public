import { all, takeLatest } from "redux-saga/effects";

import {
  ADD_STREAM,
  GET_CHANNELS,
  LOGOUT,
  SET_HOST_PEER,
  SET_HOST_SOCKET,
  SET_MEDIA_STREAM,
  SET_USER_PEER,
  SET_USER_SOCKET,
  SET_USER_STATUS,
  SET_USER_STREAM,
  SIGNIN,
  UPDATE_REMOVED_USER_STREAM,
  UPDATE_USER_REQUESTS,
  UPLOAD_FILE,
  USER_REQUEST,
  USER_REQUEST_CANCEL,
} from "../constants";
import { AppMiddleware, AuthMiddleware } from "../middlewares";

export function* Sagas() {
  yield all([
    yield takeLatest(SIGNIN, AuthMiddleware.SignIn),
    yield takeLatest(LOGOUT, AuthMiddleware.Logout),
    yield takeLatest(GET_CHANNELS, AppMiddleware.GetChannels),
    yield takeLatest(ADD_STREAM, AppMiddleware.AddStream),
    yield takeLatest(UPLOAD_FILE, AppMiddleware.UploadFile),
    yield takeLatest(SET_MEDIA_STREAM, AppMiddleware.setMediaStreams),
    yield takeLatest(SET_HOST_SOCKET, AppMiddleware.setHostSocket),
    yield takeLatest(SET_HOST_PEER, AppMiddleware.setHostPeer),
    yield takeLatest(SET_USER_SOCKET, AppMiddleware.setUserSocket),
    yield takeLatest(SET_USER_PEER, AppMiddleware.setUserPeer),
    yield takeLatest(SET_USER_STREAM, AppMiddleware.setUserStreams),
    yield takeLatest(SET_USER_STATUS, AppMiddleware.setUserStatus),
    yield takeLatest(
      UPDATE_REMOVED_USER_STREAM,
      AppMiddleware.updateRemoveUserStreams
    ),
    yield takeLatest(USER_REQUEST, AppMiddleware.userRequest),
    yield takeLatest(UPDATE_USER_REQUESTS, AppMiddleware.updateUserRequest),
  ]);
}

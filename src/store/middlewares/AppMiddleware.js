import { put } from "redux-saga/effects";

import { ApiCaller, Utils } from "../../config";
import { AppAction } from "../actions";

export default class AppMiddleware {
  static *AddStream({ payload }) {
    try {
      let res = yield ApiCaller.Post("add/stream", payload);
      if (res.status == 200) {
        // yield put(AppAction.AddStreamSuccess(res.data.data))
        yield put(AppAction.AddStreamSuccess());
        Utils.showSnackBar({ message: res?.data?.message });
        console.log("%cResponse", "color: green", " => ", res);
      } else {
        yield put(AppAction.AddStreamFailure());
        console.log("%cResponse", "color: red", " => ", res);
      }
    } catch (err) {
      yield put(AppAction.AddStreamFailure());
      console.log(`%c${err.name}`, "color: red", " => ", err);
    }
  }

  static *GetChannels() {
    try {
      let res = yield ApiCaller.Get("all/channel");
      if (res.status == 200) {
        yield put(AppAction.GetChannelsSuccess(res.data.data));
        console.log("%cResponse", "color: green", " => ", res);
      } else {
        yield put(AppAction.GetChannelsFailure());
        console.log("%cResponse", "color: red", " => ", res);
      }
    } catch (err) {
      yield put(AppAction.GetChannelsFailure());
      console.log(`%c${err.name}`, "color: red", " => ", err);
    }
  }

  static *setMediaStreams({ payload }) {
    try {
      // yield put(AppAction.ChangeSocketRef(payload));
      // yield delay(2000);
      yield put(AppAction.setMediaStreamSuccess(payload));
      console.log("%cResponse", "color: green", " => socket ref saved");
    } catch (err) {
      yield put(AppAction.setMediaStreamFailure());
      console.log(`%c${err.name}`, "color: red", " => socket ref error");
    }
  }

  static *UploadFile({ payload }) {
    try {
      let res = yield ApiCaller.Post(
        "upload/stream_video",
        payload
        // , {
        //   user_id,
        //   Authorization: token,
        //   "Content-Type": "multipart/form-data",
        //   Accept: "multipart/form-data",
        // }
      );
      if (res.status == 200) {
        yield put(AppAction.UploadFileSuccess());
        Utils.showSnackBar({ message: res?.data?.message });
        console.log("%cResponse", "color: green", " => ", res);
      } else {
        yield put(AppAction.UploadFileFailure());
        console.log("%cResponse", "color: red", " => ", res);
      }
    } catch (err) {
      yield put(AppAction.UploadFileFailure());
      console.log(`%c${err.name}`, "color: red", " => ", err);
    }
  }

  static *setHostSocket({ payload }) {
    try {
      // yield put(AppAction.ChangeSocketRef(payload));
      // yield delay(2000);
      yield put(AppAction.setHostSocketSuccess(payload));
      console.log("%cResponse", "color: green", " => socket ref saved");
    } catch (err) {
      yield put(AppAction.setHostSocketFailure());
      console.log(`%c${err.name}`, "color: red", " => socket ref error");
    }
  }

  static *setHostPeer({ payload }) {
    try {
      // yield put(AppAction.changeRTCPeerConnection(payload));
      // yield delay(2000);
      yield put(AppAction.setHostPeerSuccess(payload));
      console.log("%cResponse", "color: green", " => socket ref saved");
    } catch (err) {
      yield put(AppAction.setHostPeerFailure());
      console.log(`%c${err.name}`, "color: red", " => socket ref error");
    }
  }

  static *setUserSocket({ payload }) {
    try {
      // yield put(AppAction.ChangeSocketRef(payload));
      // yield delay(2000);
      yield put(AppAction.setUserSocketSuccess(payload));
      console.log("%cResponse", "color: green", " => socket ref saved");
    } catch (err) {
      yield put(AppAction.setUserSocketFailure());
      console.log(`%c${err.name}`, "color: red", " => socket ref error");
    }
  }

  static *setUserPeer({ payload }) {
    try {
      // yield put(AppAction.changeRTCPeerConnection(payload));
      // yield delay(2000);
      yield put(AppAction.setUserPeerSuccess(payload));
      console.log("%cResponse", "color: green", " => socket ref saved");
    } catch (err) {
      yield put(AppAction.setUserPeerFailure());
      console.log(`%c${err.name}`, "color: red", " => socket ref error");
    }
  }

  static *setUserStreams({ payload }) {
    try {
      // yield put(AppAction.ChangeSocketRef(payload));
      // yield delay(2000);
      yield put(AppAction.setUserStreamSuccess(payload));
      console.log("%cResponse", "color: green", " => socket ref saved");
    } catch (err) {
      yield put(AppAction.setUserStreamFailure());
      console.log(`%c${err.name}`, "color: red", " => socket ref error");
    }
  }

  static *updateRemoveUserStreams({ payload, userId }) {
    try {
      // yield put(AppAction.ChangeSocketRef(payload));
      // yield delay(2000);
      yield put(AppAction.updateRemoveUserStreamSuccess(payload));
      yield put(AppAction.updateUserRequest(userId));
      console.log("%cResponse", "color: green", " => socket ref saved");
    } catch (err) {
      yield put(AppAction.updateRemoveUserStreamFailure());
      console.log(`%c${err.name}`, "color: red", " => socket ref error");
    }
  }

  static *setUserStatus({ payload }) {
    try {
      // yield put(AppAction.ChangeSocketRef(payload));
      // yield delay(2000);
      yield put(AppAction.setUserStatusSuccess(payload));
      console.log("%cResponse", "color: green", " => socket ref saved");
    } catch (err) {
      yield put(AppAction.setUserStatusFailure());
      console.log(`%c${err.name}`, "color: red", " => socket ref error");
    }
  }

  static *userRequest({ payload }) {
    try {
      // yield put(AppAction.ChangeSocketRef(payload));
      // yield delay(2000);
      yield put(AppAction.userRequestSuccess(payload));
      console.log("%cResponse", "color: green", " => socket ref saved");
    } catch (err) {
      yield put(AppAction.userRequestFailure());
      console.log(`%c${err.name}`, "color: red", " => socket ref error");
    }
  }

  static *updateUserRequest({ payload }) {
    try {
      // yield put(AppAction.ChangeSocketRef(payload));
      // yield delay(2000);
      yield put(AppAction.updateUserRequestSuccess(payload));
      console.log("%cResponse", "color: green", " => socket ref saved");
    } catch (err) {
      yield put(AppAction.updateUserRequestFailure());
      console.log(`%c${err.name}`, "color: red", " => socket ref error");
    }
  }
}

import { delay, put } from "redux-saga/effects";

import { ApiCaller, Utils } from "../../config";
import { AuthAction } from "../actions";

export default class AuthMiddleware {
  static *SignIn({ payload, cb }) {
    console.log("🚀 ~ file: AuthMiddleware.js ~ line 8 ~ AuthMiddleware ~ *SignIn ~ payload", payload)
    try {
      // let res = yield ApiCaller.Post(
      //     "signin",
      //     payload
      // )
      // if (res.status == 200) {
      //     yield put(AuthAction.SignInSuccess(res.data))
      //     console.log('%cSignIn Response', "color: green", ' => ', res)
      // }
      // else {
      //     yield put(AuthAction.SignInFailure())
      //     console.log('%cSignIn Response', "color: red", ' => ', res)
      // }
      yield delay(2000);
      //   yield put(AuthAction.SignInSuccess({isLoggedIn:}));
      //   yield localStorage.setItem(
      //     "user",
      //     JSON.stringify({ auth: { isLoggedIn: true } })
      //   );
      yield put(AuthAction.SignInSuccess(payload));
      // Utils.showSnackBar({ message: "Successfully login" });
    } catch (err) {
      yield put(AuthAction.SignInFailure());
      console.log(`%c${err.name}`, "color: red", " => ", err);
    }
  }

  static *Signup({ payload, cb }) {
    try {
      let res = yield ApiCaller.Post("signup", payload);
      if (res.status == 200) {
        yield put(AuthAction.SignupSuccess(res.data));
        console.log("%cSignIn Response", "color: green", " => ", res);
      } else {
        yield put(AuthAction.SignupFailure());
        console.log("%cSignIn Response", "color: red", " => ", res);
      }
      yield delay(2000);
      //   yield put(AuthAction.SignInSuccess({isLoggedIn:}));
      //   yield localStorage.setItem(
      //     "user",
      //     JSON.stringify({ auth: { isLoggedIn: true } })
      //   );
      yield put(AuthAction.SignupSuccess(payload));
      // Utils.showSnackBar({ message: "Successfully login" });
    } catch (err) {
      yield put(AuthAction.SignupFailure());
      console.log(`%c${err.name}`, "color: red", " => ", err);
    }
  }

  static *Logout({ cb }) {
    try {
      // let res = yield ApiCaller.Post(
      //     "logout",
      //     payload
      // )
      // if (res.status == 200) {
      //     yield put(AuthAction.LogoutSuccess())
      //     console.log('%Logout Response', "color: green", ' => ', res)
      // } else {
      //     yield put(AuthAction.LogoutFailure())
      //     console.log('%Logout Response', "color: red", ' => ', res)
      // }
      yield delay(1000);
      localStorage.removeItem("user");
      yield put(AuthAction.LogoutSuccess());
      // Utils.showSnackBar({ message: "Successfully logout" });
      cb();
    } catch (err) {
      yield put(AuthAction.LogoutFailure());
      console.log(`%c${err.name}`, "color: red", " => ", err);
    }
  }

  static *ForgotPassword({ payload, cb }) {
    try {
      // let res = yield ApiCaller.Post("/auth/forget-password", payload);
      // if (res.status == 200) {
      //   yield put(AuthAction.forgotPasswordSuccess(res.data.data));
      //   cb();
      // } else {
      //   yield put(AuthAction.forgotPasswordFailure(res.data.error));
      //   yield delay(3000);
      //   yield put(AuthAction.removeError());
      // }
      yield put(AuthAction.LoaderTrue());
      yield delay(3000);
      yield put(AuthAction.LoaderFalse());
      cb();
    } catch (err) {
      // yield put(AuthAction.forgotPasswordFailure(err));
      // yield delay(3000);
      // yield put(AuthAction.removeError());
    }
  }
}

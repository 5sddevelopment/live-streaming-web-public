import { createStore, applyMiddleware } from "redux";
import RootReducer from "./reducers";
import createSagaMiddleware from "redux-saga";
import { createLogger } from "redux-logger";
import { Sagas } from "./sagas/Sagas";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
  key: "root",
  storage,
  whitelist: ['auth'] ,
};

const persistedReducer = persistReducer(persistConfig, RootReducer);

const store = createStore(
    persistedReducer,
  {},
  applyMiddleware(createLogger(), sagaMiddleware)
);

let persistor = persistStore(store)

sagaMiddleware.run(Sagas);

export  { store, persistor };

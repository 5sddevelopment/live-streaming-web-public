import {
  ADD_STREAM,
  ADD_STREAM_FAILURE,
  ADD_STREAM_SUCCESS,
  GET_CHANNELS,
  GET_CHANNELS_FAILURE,
  GET_CHANNELS_SUCCESS,
  GET_RECORDINGS,
  GET_RECORDINGS_FAILURE,
  GET_RECORDINGS_SUCCESS,
  LOADER_FALSE,
  LOADER_TRUE,
  SET_HOST_PEER,
  SET_HOST_PEER_FAILURE,
  SET_HOST_PEER_SUCCESS,
  SET_HOST_SOCKET,
  SET_HOST_SOCKET_FAILURE,
  SET_HOST_SOCKET_SUCCESS,
  SET_MEDIA_STREAM,
  SET_MEDIA_STREAM_FAILURE,
  SET_MEDIA_STREAM_SUCCESS,
  SET_USER_PEER,
  SET_USER_PEER_FAILURE,
  SET_USER_PEER_SUCCESS,
  SET_USER_SOCKET,
  SET_USER_SOCKET_FAILURE,
  SET_USER_SOCKET_SUCCESS,
  SET_USER_STATUS,
  SET_USER_STATUS_FAILURE,
  SET_USER_STATUS_SUCCESS,
  SET_USER_STREAM,
  SET_USER_STREAM_FAILURE,
  SET_USER_STREAM_SUCCESS,
  UPDATE_REMOVED_USER_STREAM,
  UPDATE_REMOVED_USER_STREAM_FAILURE,
  UPDATE_REMOVED_USER_STREAM_SUCCESS,
  UPDATE_USER_REQUESTS,
  UPDATE_USER_REQUESTS_FAILURE,
  UPDATE_USER_REQUESTS_SUCCESS,
  UPLOAD_FILE,
  UPLOAD_FILE_FAILURE,
  UPLOAD_FILE_SUCCESS,
  USER_REQUEST,
  USER_REQUEST_FAILURE,
  USER_REQUEST_SUCCESS,
} from "../constants";

export default class AppAction {
  static AddStream(payload) {
    return {
      type: ADD_STREAM,
      payload,
    };
  }
  static AddStreamSuccess(payload) {
    return {
      type: ADD_STREAM_SUCCESS,
      payload,
    };
  }
  static AddStreamFailure(payload) {
    return {
      type: ADD_STREAM_FAILURE,
      payload,
    };
  }

  static GetChannels() {
    return {
      type: GET_CHANNELS,
    };
  }
  static GetChannelsSuccess(payload) {
    return {
      type: GET_CHANNELS_SUCCESS,
      payload,
    };
  }
  static GetChannelsFailure(payload) {
    return {
      type: GET_CHANNELS_FAILURE,
      payload,
    };
  }
  static GetRecordings() {
    return {
      type: GET_RECORDINGS,
    };
  }
  static GetRecordingsSuccess(payload) {
    return {
      type: GET_RECORDINGS_SUCCESS,
      payload,
    };
  }
  static GetRecordingsFailure(payload) {
    return {
      type: GET_RECORDINGS_FAILURE,
      payload,
    };
  }

  static UploadFile(payload) {
    return {
      type: UPLOAD_FILE,
      payload,
    };
  }
  static UploadFileSuccess(payload) {
    return {
      type: UPLOAD_FILE_SUCCESS,
      payload,
    };
  }
  static UploadFileFailure(payload) {
    return {
      type: UPLOAD_FILE_FAILURE,
      payload,
    };
  }
  static setHostPeer(payload) {
    return {
      type: SET_HOST_PEER,
      payload,
    };
  }
  static setHostPeerSuccess(payload) {
    return {
      type: SET_HOST_PEER_SUCCESS,
      payload,
    };
  }
  static setHostPeerFailure(payload) {
    return {
      type: SET_HOST_PEER_FAILURE,
      payload,
    };
  }
  static setHostSocket(payload) {
    return {
      type: SET_HOST_SOCKET,
      payload,
    };
  }
  static setHostSocketSuccess(payload) {
    return {
      type: SET_HOST_SOCKET_SUCCESS,
      payload,
    };
  }
  static setHostSocketFailure(payload) {
    return {
      type: SET_HOST_SOCKET_FAILURE,
      payload,
    };
  }
  static setUserPeer(payload) {
    return {
      type: SET_USER_PEER,
      payload,
    };
  }
  static setUserPeerSuccess(payload) {
    return {
      type: SET_USER_PEER_SUCCESS,
      payload,
    };
  }
  static setUserPeerFailure(payload) {
    return {
      type: SET_USER_PEER_FAILURE,
      payload,
    };
  }
  static setUserSocket(payload) {
    return {
      type: SET_USER_SOCKET,
      payload,
    };
  }
  static setUserSocketSuccess(payload) {
    return {
      type: SET_USER_SOCKET_SUCCESS,
      payload,
    };
  }
  static setUserSocketFailure(payload) {
    return {
      type: SET_USER_SOCKET_FAILURE,
      payload,
    };
  }
  static setMediaStream(payload) {
    return {
      type: SET_MEDIA_STREAM,
      payload,
    };
  }
  static setMediaStreamSuccess(payload) {
    return {
      type: SET_MEDIA_STREAM_SUCCESS,
      payload,
    };
  }
  static setMediaStreamFailure(payload) {
    return {
      type: SET_MEDIA_STREAM_FAILURE,
      payload,
    };
  }
  static setUserStream(payload) {
    return {
      type: SET_USER_STREAM,
      payload,
    };
  }
  static setUserStreamSuccess(payload) {
    return {
      type: SET_USER_STREAM_SUCCESS,
      payload,
    };
  }
  static setUserStreamFailure(payload) {
    return {
      type: SET_USER_STREAM_FAILURE,
      payload,
    };
  }
  static updateRemoveUserStream(payload, userId) {
    return {
      type: UPDATE_REMOVED_USER_STREAM,
      payload,
      userId,
    };
  }
  static updateRemoveUserStreamSuccess(payload) {
    return {
      type: UPDATE_REMOVED_USER_STREAM_SUCCESS,
      payload,
    };
  }
  static updateRemoveUserStreamFailure(payload) {
    return {
      type: UPDATE_REMOVED_USER_STREAM_FAILURE,
      payload,
    };
  }
  static setUserStatus(payload) {
    return {
      type: SET_USER_STATUS,
      payload,
    };
  }
  static setUserStatusSuccess(payload) {
    return {
      type: SET_USER_STATUS_SUCCESS,
      payload,
    };
  }
  static setUserStatusFailure(payload) {
    return {
      type: SET_USER_STATUS_FAILURE,
      payload,
    };
  }
  static userRequest(payload) {
    return {
      type: USER_REQUEST,
      payload,
    };
  }
  static userRequestSuccess(payload) {
    return {
      type: USER_REQUEST_SUCCESS,
      payload,
    };
  }
  static userRequestFailure(payload) {
    return {
      type: USER_REQUEST_FAILURE,
      payload,
    };
  }
  static updateUserRequest(payload) {
    return {
      type: UPDATE_USER_REQUESTS,
      payload,
    };
  }
  static updateUserRequestSuccess(payload) {
    return {
      type: UPDATE_USER_REQUESTS_SUCCESS,
      payload,
    };
  }
  static updateUserRequestFailure(payload) {
    return {
      type: UPDATE_USER_REQUESTS_FAILURE,
      payload,
    };
  }
  static LoaderTrue() {
    return {
      type: LOADER_TRUE,
    };
  }
  static LoaderFalse() {
    return {
      type: LOADER_FALSE,
    };
  }
}

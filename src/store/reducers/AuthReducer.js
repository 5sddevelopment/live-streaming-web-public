import {
  LOADER_FALSE,
  LOADER_TRUE,
  LOGOUT,
  LOGOUT_FAILURE,
  LOGOUT_SUCCESS,
  SIGNIN,
  SIGNIN_FAILURE,
  SIGNIN_SUCCESS,
} from "../constants";

const initialState = {
  isLoggedIn: localStorage.getItem("authUser") ? true : false,
  user: localStorage.getItem("user")
    ? JSON.parse(localStorage.getItem("user"))
    : {},
  isLoading: false,
  error: null,
};

export default function AuthReducer(state = initialState, action) {
  switch (action.type) {
    case SIGNIN:
      state = {
        ...state,
        isLoading: true,
      };
      break;
    case SIGNIN_SUCCESS:
      state = {
        ...state,
        user: { ...action.payload },
        isLoggedIn: true,
        isLoading: false,
      };
      break;
    case SIGNIN_FAILURE:
      state = {
        ...state,
        isLoading: false,
      };
      break;

    case LOGOUT:
      state = {
        ...state,
        isLoading: true,
      };
      break;
    case LOGOUT_SUCCESS:
      state = {
        user: {},
        isLoggedIn: false,
        isLoading: false,
      };
      break;
    case LOGOUT_FAILURE:
      state = {
        ...state,
        isLoading: false,
      };
      break;
    case LOADER_TRUE:
      state = {
        ...state,
        isLoading: true,
      };
      break;

    case LOADER_FALSE:
      state = {
        ...state,
        isLoading: false,
      };
      break;

    default:
      break;
  }

  return state;
}

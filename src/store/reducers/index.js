import { combineReducers } from "redux";
import AuthReducer from "./AuthReducer";
import AppReducer from "./AppReducer";
const RootReducer = combineReducers({
  auth: AuthReducer,
  app: AppReducer,
});

export default RootReducer;

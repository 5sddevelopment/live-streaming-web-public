import {
  ADD_STREAM,
  ADD_STREAM_FAILURE,
  GET_CHANNELS,
  GET_CHANNELS_FAILURE,
  GET_CHANNELS_SUCCESS,
  GET_RECORDINGS,
  GET_RECORDINGS_FAILURE,
  GET_RECORDINGS_SUCCESS,
  LOADER_FALSE,
  LOADER_TRUE,
  SET_HOST_PEER,
  SET_HOST_PEER_FAILURE,
  SET_HOST_PEER_SUCCESS,
  SET_HOST_SOCKET,
  SET_HOST_SOCKET_FAILURE,
  SET_HOST_SOCKET_SUCCESS,
  SET_MEDIA_STREAM,
  SET_MEDIA_STREAM_FAILURE,
  SET_USER_PEER,
  SET_USER_PEER_FAILURE,
  SET_USER_SOCKET,
  SET_USER_SOCKET_FAILURE,
  SET_USER_STATUS,
  SET_USER_STATUS_FAILURE,
  SET_USER_STATUS_SUCCESS,
  SET_USER_STREAM,
  SET_USER_STREAM_FAILURE,
  SET_USER_STREAM_SUCCESS,
  UPDATE_REMOVED_USER_STREAM,
  UPDATE_REMOVED_USER_STREAM_FAILURE,
  UPDATE_REMOVED_USER_STREAM_SUCCESS,
  UPDATE_USER_REQUESTS,
  UPDATE_USER_REQUESTS_FAILURE,
  UPDATE_USER_REQUESTS_SUCCESS,
  USER_REQUEST,
  USER_REQUEST_FAILURE,
  USER_REQUEST_SUCCESS,
} from "../constants";

const initialState = {
  user: localStorage.getItem("user")
    ? JSON.parse(localStorage.getItem("user"))
    : {},
  loader: false,
  userRequests: [],
  userStreams: [],
  liveUserStreams: [],
  hostSocket: null,
  hostPeerConnection: null,
  error: null,
};

export default function AppReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_STREAM:
    case GET_CHANNELS:
    case GET_RECORDINGS:
    case SET_HOST_PEER:
    case SET_HOST_SOCKET:
    case SET_USER_PEER:
    case SET_USER_SOCKET:
    case SET_MEDIA_STREAM:
    case SET_USER_STREAM:
    case UPDATE_REMOVED_USER_STREAM:
    case SET_USER_STATUS:
    case USER_REQUEST:
    case UPDATE_USER_REQUESTS:
      state = {
        ...state,
        loader: true,
        error: null,
      };
      break;
    // case ADD_STREAM_SUCCESS:
    //   state = {
    //     ...state,
    //     channels: [action.payload, ...state.channels],
    //     loader: false,
    //   };
    //   break;
    case GET_CHANNELS_SUCCESS:
      state = {
        ...state,
        channels: action.payload,
        loader: false,
      };
      break;
    case GET_RECORDINGS_SUCCESS:
      state = {
        ...state,
        recordedStreams: action.payload,
        loader: false,
      };
      break;
    case SET_HOST_SOCKET_SUCCESS:
      state = {
        ...state,
        hostSocket: action.payload,
        loader: false,
      };
      break;
    case SET_HOST_PEER_SUCCESS:
      state = {
        ...state,
        hostPeerConnection: action.payload,
        loader: false,
      };
      break;
    // case SET_USER_SOCKET_SUCCESS:
    //   state = {
    //     ...state,
    //     userSocket: [...state.userSocket, action.payload],
    //     loader: false,
    //   };
    //   break;
    // case SET_USER_PEER_SUCCESS:
    //   state = {
    //     ...state,
    //     userPeerConnection: [...state.userPeerConnection, action.payload],
    //     loader: false,
    //   };
    //   break;
    // case SET_MEDIA_STREAM_SUCCESS:
    //   state = {
    //     ...state,
    //     mediaStreams: [...state.mediaStreams, action.payload],
    //     loader: false,
    //   };
    //   break;
    case SET_USER_STREAM_SUCCESS:
      let userStreams = [];
      if (action.payload) {
        userStreams = state.userStreams.filter(
          (val) => val.socketId !== action.payload.socketId
        );
        userStreams.push(action.payload);
      } else {
        userStreams = [];
      }

      state = {
        ...state,
        userStreams,
        loader: false,
      };
      break;
    case UPDATE_REMOVED_USER_STREAM_SUCCESS:
      let updatedUserStreams = [];
      let liveUpdatedUserStreams = [];
      const streamFound = state.userStreams.find(
        (item) => item.socketId === action.payload
      );
      if (streamFound) {
        updatedUserStreams = state.userStreams.filter(
          (item) => item.socketId !== action.payload
        );
        liveUpdatedUserStreams = state.liveUserStreams;
      } else {
        liveUpdatedUserStreams = state.liveUserStreams.filter(
          (item) => item.socketId !== action.payload
        );
        updatedUserStreams = state.userStreams;
      }

      state = {
        ...state,
        userStreams: updatedUserStreams,
        liveUserStreams: liveUpdatedUserStreams,
        loader: false,
      };
      break;
    case SET_USER_STATUS_SUCCESS:
      let waitingUserStreams = [];
      let liveUserStreams = [];

      if (action.payload.status) {
        liveUserStreams = [
          ...state.liveUserStreams,
          ...state.userStreams.filter(
            (item) => item.socketId === action.payload.socketId
          ),
        ];
        waitingUserStreams = state.userStreams.filter(
          (item) => item.socketId !== action.payload.socketId
        );
      } else {
        const streamFound = state.userStreams.find(
          (item) => item.socketId === action.payload.socketId
        );
        if (streamFound) {
          waitingUserStreams = state.userStreams.filter(
            (item) => item.socketId !== action.payload.socketId
          );
          liveUserStreams = state.liveUserStreams;
        } else {
          liveUserStreams = state.liveUserStreams.filter(
            (item) => item.socketId !== action.payload.socketId
          );
          waitingUserStreams = state.userStreams;
        }
      }

      state = {
        ...state,
        userStreams: waitingUserStreams,
        liveUserStreams: liveUserStreams,
        loader: false,
      };
      break;
    case USER_REQUEST_SUCCESS:
      const newUser = action.payload;
      state = {
        ...state,
        userRequests: [...state.userRequests, newUser],
        loader: false,
      };
      break;
    case UPDATE_USER_REQUESTS_SUCCESS:
      state = {
        ...state,
        userRequests: state.userRequests.filter(
          (item) => item.userId !== action.payload
        ),
        loader: false,
      };
      break;
    case ADD_STREAM_FAILURE:
    case GET_CHANNELS_FAILURE:
    case GET_RECORDINGS_FAILURE:
    case SET_HOST_PEER_FAILURE:
    case SET_HOST_SOCKET_FAILURE:
    case SET_USER_PEER_FAILURE:
    case SET_USER_SOCKET_FAILURE:
    case SET_MEDIA_STREAM_FAILURE:
    case SET_USER_STREAM_FAILURE:
    case UPDATE_REMOVED_USER_STREAM_FAILURE:
    case SET_USER_STATUS_FAILURE:
    case USER_REQUEST_FAILURE:
    case UPDATE_USER_REQUESTS_FAILURE:
      state = {
        ...state,
        loader: false,
        error: action.payload,
      };
      break;
    case LOADER_TRUE:
      state = {
        ...state,
        loader: true,
      };
      break;

    case LOADER_FALSE:
      state = {
        ...state,
        loader: false,
      };
      break;

    default:
      break;
  }

  return state;
}
